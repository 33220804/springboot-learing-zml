package com.zml.springboot.web;

import com.zml.springboot.bean.User;
import com.zml.springboot.bean.User2;
import com.zml.springboot.bean.User3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Title :
 * Description:
 *
 * @Auter : ZML
 * @created : 2019/6/25 0025
 */
@Controller
public class ConfigController {

    @Value("${user.account}")
    private String useraccount;
    @Value("${user.age}")
    private int userage;

    /**
     * 读取application.propertie和yml的中文乱码问题
     * @return
     */
    @RequestMapping(value = "/user",method = RequestMethod.GET)
    @ResponseBody
    public String getuserinfo() {
        return "姓名："+useraccount+"  年龄："+userage;
    }


    /**
     * 读取赋值到实体类中的值
     */
    @Autowired
    private  User user1;
    @RequestMapping(value = "/user1",method = RequestMethod.GET)
    @ResponseBody
    public User getuser1() {
        return user1;
    }

    /**
     *  添加前缀读取
     */
    @Autowired
    private User2 user2;
    @RequestMapping(value = "/user2",method = RequestMethod.GET)
    @ResponseBody
    public User2 getuser2() {
       return  user2;
    }

    /**
     * 读取自定义配置文件
     */
    @Autowired
    private User3 user3;
    @RequestMapping(value = "/user3",method = RequestMethod.GET)
    @ResponseBody
    public User3 getuser3() {
        return  user3;
    }

}
