package com.zml.springboot.bean;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

/**
 * Title :
 * Description:
 *      读取自定义配置文件
 * @Auter : ZML
 * @created : 2019/6/25 0025
 */
@Component
@ConfigurationProperties(prefix = "user3")
//@PropertySource(value = "classpath:userdata.properties")
@PropertySource(value = "classpath:userdata.yml")
public class User3 {
    private String account;
    private int age;

    /**
     * PropertySource只能读取properties文件，
     *  需读取yml则注入如下bean
     * @return
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer properties() {
        PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
        YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
    //  yaml.setResources(new ClassPathResource("myconfig.yml"));
    //  yaml.setResources(new FileSystemResource("/data/config/myconfig.yml"));
        yaml.setResources(new ClassPathResource("userdata.yml"));
        configurer.setProperties(yaml.getObject());
        return configurer;
    }
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
