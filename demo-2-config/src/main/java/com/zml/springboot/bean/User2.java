package com.zml.springboot.bean;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Title :
 * Description:
 *      添加前缀读取
 * @Auter : ZML
 * @created : 2019/6/25 0025
 */
@Component
//@ConfigurationProperties(prefix = "user2")
public class User2 {
    private String account;
    private int age;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
