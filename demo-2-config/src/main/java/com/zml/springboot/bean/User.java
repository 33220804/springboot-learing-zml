package com.zml.springboot.bean;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Title : User实体类
 * Description:
 *      读取application赋值
 * @Auter : ZML
 * @created : 2019/6/25 0025
 */

@Component
public class User {
    //@Value("${user1.account}")
    private String useraccount;
    //@Value("${user1.age}")
    private int userage;

    public String getUseraccount() {
        return useraccount;
    }

    public void setUseraccount(String useraccount) {
        this.useraccount = useraccount;
    }

    public int getUserage() {
        return userage;
    }

    public void setUserage(int userage) {
        this.userage = userage;
    }
}
