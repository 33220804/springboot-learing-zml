package com.zml.springboot.controller;

import com.zml.springboot.model.User;
import com.zml.springboot.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Title : restfull
 * Description:
 *      get:   /users  查询用户列表
 *      post:  /users  创建用户
 *      get:   /users/id   根据id查询用户
 *      put: /users/id     根据id更新用户
 *      delete: /users/id  根据id删除用户
 * @Auter : ZML
 * @created : 2019/6/25 0025
 */
@Api("用户api")
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 用户列表
     * @return
     */
    @ApiOperation(value="获取用户列表", notes="")
    @RequestMapping(method = RequestMethod.GET)
     public List<User> getUserList(){
        return userService.getAll();
    }

    /**
     * 创建用户
     * @param user
     * @return
     */
    @ApiOperation(value="创建用户", notes="根据User对象创建用户")
    @ApiImplicitParam(name = "user", value = "用户详细实体user", required = true, dataType = "User")
    @RequestMapping( method=RequestMethod.POST)
    public String postUser(@ModelAttribute User user) {
        userService.addUser(user);
        return "success";
    }

    /**
     * id查询用户
     * @param id
     * @return
     */
    @ApiOperation(value="获取用户详细信息", notes="根据url的id来获取用户详细信息")
    @ApiImplicitParam(name = "id", value = "用户ID", required = true, paramType="path",dataType = "Integer")
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public User getUser(@PathVariable Integer id) {
        // 处理"/users/{id}"的GET请求，用来获取url中id值的User信息
        // url中的id可通过@PathVariable绑定到函数的参数中
        return userService.getById(id);
    }

    /**
     * 更新用户
     * @param id
     * @param user
     * @return
     */
    @ApiOperation(value="更新用户详细信息", notes="根据url的id来指定更新对象，并根据传过来的user信息来更新用户详细信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "user", value = "用户详细实体user", required = true, dataType = "User")
    })
    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public String putUser(@PathVariable Integer id, @ModelAttribute User user) {
        // 处理"/users/{id}"的PUT请求，用来更新User信息
        User u =  userService.getById(id);
        u.setUsername(user.getUsername());
        u.setIdcard(user.getIdcard());
        userService.updateUser(u);
        return "success";
    }

    /**
     * 删除用户
     * @param id
     * @return
     */
    @ApiOperation(value="删除用户", notes="根据url的id来指定删除对象")
    @ApiImplicitParam(name = "id", value = "用户ID", required = true,paramType="path" ,dataType = "Integer")
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public String deleteUser(@PathVariable Integer id) {
        // 处理"/users/{id}"的DELETE请求，用来删除User
        userService.deleteUser(id);
        return "success";
    }
}
