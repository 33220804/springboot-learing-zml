package com.zml.springboot.dao;

import com.zml.springboot.model.User;

import java.util.List;

//@Mapper  方法一@Mapper  方法二 application在中@MapperScan(包名路径)@
public interface UserMapper {
    int deleteByPrimaryKey(Integer userid);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer userid);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    List<User> getAll();
}