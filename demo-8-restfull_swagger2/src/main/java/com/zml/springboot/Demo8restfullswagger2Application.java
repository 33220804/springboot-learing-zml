package com.zml.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.zml.springboot.dao")
public class Demo8restfullswagger2Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo8restfullswagger2Application.class, args);
    }

}
