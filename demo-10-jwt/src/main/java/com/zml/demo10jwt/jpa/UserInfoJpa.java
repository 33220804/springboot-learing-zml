package com.zml.demo10jwt.jpa;

import com.zml.demo10jwt.model.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Title :
 * Description:
 *
 * @Auter : ZML
 * @created : 2019/7/14 0014
 */
public interface UserInfoJpa extends JpaRepository<UserInfo,String> {
}
