package com.zml.demo10jwt.jpa;

import com.zml.demo10jwt.model.TokenInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Title :
 * Description:
 *
 * @Auter : ZML
 * @created : 2019/7/14 0014
 */
public interface TokenJpa extends JpaRepository<TokenInfo,String> ,JpaSpecificationExecutor<TokenInfo> {
}
