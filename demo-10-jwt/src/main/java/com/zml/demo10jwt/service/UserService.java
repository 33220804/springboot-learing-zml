package com.zml.demo10jwt.service;

import com.zml.demo10jwt.model.User;

import java.util.List;

/**
 * Title :
 * Description:
 *
 * @Auter : ZML
 * @created : 2019/6/25 0025
 */

public interface UserService {

    List<User> getAll();
}
