package com.zml.demo10jwt.service.impl;

import com.zml.demo10jwt.dao.UserMapper;
import com.zml.demo10jwt.model.User;
import com.zml.demo10jwt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Title :
 * Description:
 *
 * @Auter : ZML
 * @created : 2019/6/25 0025
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public List<User> getAll() {
        return userMapper.getAll();
    }
}
