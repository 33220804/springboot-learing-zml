package com.zml.demo10jwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo10JwtApplication {

    public static void main(String[] args) {
        SpringApplication.run(Demo10JwtApplication.class, args);
    }

}
