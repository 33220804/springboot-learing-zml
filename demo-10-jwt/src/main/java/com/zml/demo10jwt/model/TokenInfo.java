package com.zml.demo10jwt.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Title :
 * Description:
 *
 * @Auter : ZML
 * @created : 2019/7/14 0014
 */
@Entity
@Table(name = "t_token_info",schema = "jwt")
public class TokenInfo implements Serializable
{
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @Column(name = "app_id")
    private  String appId;
    @Column(name = "token")
    private byte[] token;
    @Column(name = "build_time")
    private String buildTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public byte[] getToken() {
        return token;
    }

    public void setToken(byte[] token) {
        this.token = token;
    }

    public String getBuildTime() {
        return buildTime;
    }

    public void setBuildTime(String buildTime) {
        this.buildTime = buildTime;
    }
}
