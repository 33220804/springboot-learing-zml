package com.zml.demo10jwt.model;

import java.io.Serializable;

/**
 * Title :
 * Description:
 *
 * @Auter : ZML
 * @created : 2019/7/14 0014
 */
public class TokenResult implements Serializable {
    private boolean flag =true;
    private  String msg ="";
    private  String token ="";

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
