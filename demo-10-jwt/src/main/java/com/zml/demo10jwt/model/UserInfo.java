package com.zml.demo10jwt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Title :
 * Description:
 *
 * @Auter : ZML
 * @created : 2019/7/14 0014
 */
@Entity
@Table(name = "t_user_info",schema = "jwt")
public class UserInfo implements Serializable
{
    @Id
    @Column(name = "app_id")
    private String appId;

    @Column(name = "app_secret")
    private byte[] appSecret;

    @Column(name = "status")
    private int status;

    @Column(name = "day_request_count")
    private int dayRequestCount;

    @Column(name = "ajax_bind_ip")
    private String ajaxBindIp;

    @Column(name = "mark")
    private String mark;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public byte[] getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(byte[] appSecret) {
        this.appSecret = appSecret;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDayRequestCount() {
        return dayRequestCount;
    }

    public void setDayRequestCount(int dayRequestCount) {
        this.dayRequestCount = dayRequestCount;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getAjaxBindIp() {
        return ajaxBindIp;
    }

    public void setAjaxBindIp(String ajaxBindIp) {
        this.ajaxBindIp = ajaxBindIp;
    }
}
