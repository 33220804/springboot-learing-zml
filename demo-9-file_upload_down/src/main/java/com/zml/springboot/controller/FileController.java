package com.zml.springboot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Title :
 * Description:
 *
 * @Auter : ZML
 * @created : 2019/7/8 0008
 */
@Controller
public class FileController  {
    private static final Logger log = LoggerFactory.getLogger(FileController.class);

    @ResponseBody
    @RequestMapping("/upload")
    public String upload(HttpServletRequest request ,@RequestParam("file") MultipartFile file){
        try {
            if (file.isEmpty()) {
                return "文件为空";
            }
            // 获取文件名
            String fileName = file.getOriginalFilename(); log.info("上传的文件名为：" + fileName);
            // 获取文件的后缀名
            String suffixName = fileName.substring(fileName.lastIndexOf(".")); log.info("文件的后缀名为：" + suffixName);
            // 设置文件存储路径
           // String filePath = "\\files\\";
           // String filePath=request.getSession().getServletContext().getRealPath("/")+"files";
            String filePath ="F:\\IdeaProjects\\springboot-learing-zml\\demo-9-file_upload_down\\src\\main\\resources\\files\\";
            String path = filePath + fileName;
            File dest = new File(path);
            // 检测是否存在目录
            if (!dest.getParentFile().exists()) {
                dest.getParentFile().mkdirs();// 新建文件夹
            }
            file.transferTo(dest);// 文件写入
            return "上传成功";
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "上传失败";

    }

    @ResponseBody
    @GetMapping("/download")
    public String downloadFile(HttpServletRequest request, HttpServletResponse response) {
        String fileName = "1.txt";// 文件名
        if (fileName != null) {
            //设置文件路径
            String path="F:\\IdeaProjects\\springboot-learing-zml\\demo-9-file_upload_down\\src\\main\\resources\\files\\";

            File file = new File(path+fileName);
            //File file = new File(realPath , fileName);
            //String path = request.getServletContext().getRealPath("/")+"files";

            if (file.exists()) {
                response.setContentType("application/force-download");// 设置强制下载不打开
                response.addHeader("Content-Disposition", "attachment;fileName=" + fileName);// 设置文件名
                byte[] buffer = new byte[1024];
                FileInputStream fis = null;
                BufferedInputStream bis = null;
                try {
                    fis = new FileInputStream(file);
                    bis = new BufferedInputStream(fis);
                    OutputStream os = response.getOutputStream();
                    int i = bis.read(buffer);
                    while (i != -1) {
                        os.write(buffer, 0, i);
                        i = bis.read(buffer);
                    }
                    return "下载成功";
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (bis != null) {
                        try {
                            bis.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (fis != null) {
                        try {
                            fis.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return "下载失败";
    }
}
