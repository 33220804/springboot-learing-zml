package com.zml.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo9FileUploadDownApplication {

    public static void main(String[] args) {
        SpringApplication.run(Demo9FileUploadDownApplication.class, args);
    }

}
