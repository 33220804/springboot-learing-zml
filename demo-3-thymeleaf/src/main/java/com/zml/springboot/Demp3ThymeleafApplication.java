package com.zml.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demp3ThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(Demp3ThymeleafApplication.class, args);
    }

}
