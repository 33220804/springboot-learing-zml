package com.zml.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo7HttpclientApplication {

    public static void main(String[] args) {
        SpringApplication.run(Demo7HttpclientApplication.class, args);
    }

}
