package com.zml.demo11springsecuritybase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo11SpringSecurityBaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(Demo11SpringSecurityBaseApplication.class, args);
    }

}
