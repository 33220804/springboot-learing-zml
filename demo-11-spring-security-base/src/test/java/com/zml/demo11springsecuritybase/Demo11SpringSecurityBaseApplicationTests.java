package com.zml.demo11springsecuritybase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Demo11SpringSecurityBaseApplicationTests {

    @Test
    public void contextLoads() {
    }

}
