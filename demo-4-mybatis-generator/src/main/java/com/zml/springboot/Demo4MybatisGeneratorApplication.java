package com.zml.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.zml.springboot.dao")
public class Demo4MybatisGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(Demo4MybatisGeneratorApplication.class, args);
    }

}
