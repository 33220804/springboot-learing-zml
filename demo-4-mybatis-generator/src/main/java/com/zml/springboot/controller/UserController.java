package com.zml.springboot.controller;

import com.zml.springboot.model.User;
import com.zml.springboot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Title :
 * Description:
 *
 * @Auter : ZML
 * @created : 2019/6/25 0025
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/all")
     List<User> getAll(){
        return userService.getAll();
    }
}
