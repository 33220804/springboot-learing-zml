package com.example.demo12eurekaservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class Demo12EurekaServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(Demo12EurekaServiceApplication.class, args);
    }

}
