package com.example.demo12eurekaclient01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class Demo12EurekaClient01Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo12EurekaClient01Application.class, args);
    }

}
