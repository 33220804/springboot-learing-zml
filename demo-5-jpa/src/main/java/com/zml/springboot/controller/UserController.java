package com.zml.springboot.controller;

import com.zml.springboot.model.ResultMessage;
import com.zml.springboot.model.User;
import com.zml.springboot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Title :
 * Description:
 *
 * @Auter : ZML
 * @created : 2019/6/25 0025
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/all")
    @ResponseBody
     List<User> getAll(){
        return userService.findAll();
    }

    @RequestMapping("/all2")
    @ResponseBody
    ResultMessage getAllUser(){
        List<User> userList =  userService.findAll();
        ResultMessage resultMessage = new ResultMessage(0,"",userList.size(),userList);
        return  resultMessage;
    }
}
