package com.zml.springboot.service;

import com.zml.springboot.model.User;

import java.util.List;

/**
 * Title :
 * Description:
 *
 * @Auter : ZML
 * @created : 2019/6/25 0025
 */

public interface UserService {

    List<User> findAll();
}
