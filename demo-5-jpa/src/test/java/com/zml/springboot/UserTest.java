package com.zml.springboot;

import com.zml.springboot.dao.UserRepository;
import com.zml.springboot.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Title :
 * Description:
 *
 * @Auter : ZML
 * @created : 2019/6/26 0026
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void addUserTest(){
        User user = new User();
        //user.setId(1003);
        user.setUsername("用户33");
        user.setAccount("user333");
        user.setPassword("123456");
        user.setSex("男");
        user.setIdcard("123456789012345671");
        user.setStatus("激活");
        userRepository.save(user);
    }
    @Test
    public void Updateuser(){
        User user = userRepository.findById(1004).get();
        user.setUsername("更新用户2211333");
        userRepository.save(user);
    }

    @Test
    public void delUserTest(){
        userRepository.deleteById(1003);
    }

    @Test
    public void findAllUser(){
        List<User> userList = userRepository.findAll();
        for (int i=0;i< userList.size();i++) {
            System.out.println(userList.get(i).toString());
        }
    }

}
