package com.zml.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo1HelloApplication {

    public static void main(String[] args) {
        SpringApplication.run(Demo1HelloApplication.class, args);
    }

}
